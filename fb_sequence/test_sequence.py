import unittest

from fb_sequence.fizz_buzz_sequence import get_sequence


class FizzBuzzSequenceTestCase(unittest.TestCase):
    def test_sequence_returns_first_n_elements_as_string(self):
        self.assertEqual("1,2,fizz,4,buzz,fizz,7,8,fizz,buzz,11,fizz,13,14,buzz", get_sequence(15))
