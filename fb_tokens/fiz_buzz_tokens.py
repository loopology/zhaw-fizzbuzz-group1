def get_fizz_buzz_token(param: int):
    buzz = "buzz"
    fizz = "fizz"
    my_list = []

    def is_fizz(p):
        return p % 3 == 0

    def is_buzz(p):
        return p % 5 == 0

    if is_fizz(param):
        my_list.append(fizz)
        if is_buzz(param):
            my_list.append(buzz)
    elif is_buzz(param):
        my_list.append(buzz)

    else:
        my_list.append(str(param))

    return "_".join(my_list)
